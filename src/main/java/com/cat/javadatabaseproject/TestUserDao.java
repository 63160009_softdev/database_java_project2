/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javadatabaseproject;

import com.cat.javadatabaseproject.dao.UserDao;
import com.cat.javadatabaseproject.helper.DatabaseHelper;
import com.cat.javadatabaseproject.model.User;

/**
 *
 * @author Black Dragon
 */
public class TestUserDao {
    public static void main(String[] args) {
        UserDao userDao = new UserDao();
        
//        User newUser = new User("user 4", "password", "staff", "Female");
//        User insertedUser = userDao.save(newUser);
//        for(User u: userDao.getAll()) {
//            System.out.println(u);
//        }
        
//        User updatedUser = userDao.get("Filemes");
//        updatedUser.setGender("Female");
//        userDao.update(updatedUser);
//        userDao.get(updatedUser.getId());
//        userDao.delete(updatedUser);
        for(User u: userDao.getAllWhereOrder(" user_id < 10 ", " user_id ","ASC")) {
            System.out.println(u);
        }
        
        DatabaseHelper.close();
    }
}
