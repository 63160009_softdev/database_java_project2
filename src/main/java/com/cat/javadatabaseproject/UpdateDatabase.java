/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javadatabaseproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Black Dragon
 */
public class UpdateDatabase {

    public static void main(String[] args) {
        Connection conn = null;
        String url = "jdbc:sqlite:D-Coffee.db";
        try {
            conn = DriverManager.getConnection(url);
            System.out.println("Connection to SQLite has been establish");
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            return;
        }

        //Insert
        String sql = "UPDATE category SET category_name=? WHERE category_id=?";

        try {
            PreparedStatement psmt = conn.prepareStatement(sql);
            psmt.setString(1, "Coffee and milk");
            psmt.setInt(2, 1);
            psmt.executeUpdate();
//            ResultSet key = psmt.getGeneratedKeys();
//            key.next();
//            System.out.println("" + key.getInt(1));
            
        } catch (SQLException ex) {
            System.out.println("ERROR!!!");
        }

        //close
        if (conn
                != null) {
            try {
                conn.close();
            } catch (SQLException ex) {
                System.out.println(ex.getMessage());
            }
        }
    }
}
