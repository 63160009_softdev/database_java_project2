/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javadatabaseproject.dao;

import com.cat.javadatabaseproject.helper.DatabaseHelper;
import java.util.List;
import com.cat.javadatabaseproject.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;

/**
 *
 * @author Black Dragon
 */
public class UserDao implements Dao<User> {

    @Override
    public User get(int id) {
        User user = null;
        String sql = "SELECT * FROM User WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = User.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        return user;
    }

    public User getByName(String name) {
        User user = null;
        String sql = "SELECT * FROM User WHERE user_name=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = User.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        return user;
    }
    
    public User getByLogin(String login) {
        User user = null;
        String sql = "SELECT * FROM User WHERE user_login=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, login);
            ResultSet rs = stmt.executeQuery();

            while (rs.next()) {
                user = User.fromRS(rs);
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        return user;
    }

    @Override
    public List<User> getAll() {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM User";
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = User.fromRS(rs);

                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        return list;
    }

    @Override
    public User save(User obj) {
        String sql = "INSERT INTO user (user_login, user_name, user_gender, user_password, user_role)"
                + "VALUES(?, ?, ?, ?, ?)";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getLogin());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getGender());
            stmt.setString(4, obj.getPassword());
            stmt.setString(5, obj.getRole());
            stmt.executeUpdate();
            int id = DatabaseHelper.getInserteId(stmt);
            obj.setId(id);

        } catch (SQLException ex) {
            System.out.println("Error");
            return null;
        }
        return obj;
    }

    @Override
    public User update(User obj) {
        String sql = "UPDATE User"
                + "   SET user_login = ?, user_name = ?, user_gender = ?, user_password = ?, user_role = ?\n"
                + " WHERE user_id = ?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getLogin());
            stmt.setString(2, obj.getName());
            stmt.setString(3, obj.getGender());
            stmt.setString(4, obj.getPassword());
            stmt.setString(5, obj.getRole());
            stmt.setInt(5, obj.getId());
            int ret = stmt.executeUpdate();
            return obj;

        } catch (SQLException ex) {
            System.out.println("Error");
            return null;
        }
    }

    @Override
    public int delete(User obj) {
        String sql = "DELETE FROM User WHERE user_id=?";
        Connection conn = DatabaseHelper.getConnect();
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();
            return ret;
            
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        return -1;
    }

    @Override
    public List<User> getAllOrderBy(String orderPerimeter, String order) {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM User ORDER BY " + orderPerimeter + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = User.fromRS(rs);

                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        return list;
    }
    
    @Override
    public List<User> getAllWhereOrder(String where, String orderPerimeter, String order) {
        ArrayList<User> list = new ArrayList();
        String sql = "SELECT * FROM User WHERE " + where + " ORDER BY " + orderPerimeter + " " + order;
        Connection conn = DatabaseHelper.getConnect();
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = User.fromRS(rs);

                list.add(user);
            }
        } catch (SQLException ex) {
            System.out.println("Error");
        }
        return list;
    }
}
