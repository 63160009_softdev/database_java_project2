/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javadatabaseproject.dao;

import com.cat.javadatabaseproject.model.User;
import java.util.List;

/**
 *
 * @author Black Dragon
 */
public interface Dao <T>{
    T get(int id);
    List<T> getAll();
    T save(T obj);
    T update(T obj);
    int delete(T obj);
    List<User> getAllOrderBy(String name, String order);
    List<User> getAllWhereOrder(String where, String orderPerimeter, String order);
}
