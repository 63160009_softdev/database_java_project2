/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javadatabaseproject.service;

import com.cat.javadatabaseproject.dao.UserDao;
import com.cat.javadatabaseproject.model.User;

/**
 *
 * @author Black Dragon
 */
public class UserService {
    public User login(String login, String password) {
        UserDao userDao = new UserDao();
        User user = userDao.getByLogin(login);
        if(user.getPassword().equals(password)){
            return user;
        }
        return null;
    }
}
