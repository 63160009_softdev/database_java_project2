/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.cat.javadatabaseproject;

import com.cat.javadatabaseproject.model.User;
import com.cat.javadatabaseproject.service.UserService;

/**
 *
 * @author Black Dragon
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService userService = new UserService();
        User user = userService.login("admin", "1234");
        if(user != null) {
            System.out.println("Welcome " + user.getName());
        }else{
            System.out.println("Incorrect username or password.");
        }
    }
}
